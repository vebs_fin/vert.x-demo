<html>
<head><title>${context.title}</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/css/bootstrap.min.css"/>
    <script type="text/javascript" src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/js/bootstrap.bundle.min.js"></script>
    <style>
        .form-center{
            margin-left: 30%;
            margin-top : 5%;
        }
        /*.form-border{*/
        /*border: 2px solid green;*/
        /*}*/
    </style>
</head>
<body>
<form action="${context.request().path()}/saveBook" method="post" class="form-center">
    <div class="form-border">
        <div class="row">
            <div class="col-sm-5">
                <label>Book Name</label>
                <input type="text" class="form-control" id="bookName" name="bookName"/>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-5">
                <label>Author</label>
                <input type="text" class="form-control" id="author" name="author"/>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-5">
                <label>Publisher</label>
                <input type="text" class="form-control" id="publisher_id" name="publisher"/>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-5">
                <label>Price</label>
                <input type="text" class="form-control" id="price" name="bookPrice"/>
            </div>
        </div>
        <div class="row">
            <div class="button">
                <button type="submit" class="btn btn-primary" style="margin-top:27%;margin-left:23%;">Submit</button>
            </div>
        </div>
    </div>
</form>

<form action="${context.request().path()}/editBook" method="post" class="form-center">
    <div class="form-border">
        <div class="row">
            <div class="col-sm-5">
                <label>Book ID</label>
                <input type="text" class="form-control" id="bookID" name="bookID"/>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-5">
                <label>Field Name</label>
                <input type="text" class="form-control" id="fieldName" name="fieldName"/>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-5">
                <label>Field Value</label>
                <input type="text" class="form-control" id="fieldValue" name="fieldValue"/>
            </div>
        </div>
        <div class="row">
            <div class="button">
                <button type="submit" class="btn btn-primary" style="margin-top:27%;margin-left:23%;">Edit</button>
            </div>
        </div>
    </div>
</form>

<form action="${context.request().path()}/deleteBook" method="post" class="form-center">
    <div class="form-border">
        <div class="row">
            <div class="col-sm-5">
                <label>Book ID</label>
                <input type="text" class="form-control" id="bookID" name="bookID"/>
            </div>
        </div>
        <div class="row">
            <div class="button">
                <button type="submit" class="btn btn-primary" style="margin-top:27%;margin-left:23%;">Delete</button>
            </div>
        </div>
    </div>
</form>
</body>
</html>