package rohit.myverticle

import io.vertx.core.AbstractVerticle
import io.vertx.core.Future

/**
 * Created by nexthought on 12/15/17.
 */
class MyVerticle extends AbstractVerticle {

    @Override
    public void start(Future<Void> startFuture) {
        System.out.println("MyVerticle started!");
    }

    @Override
    public void stop(Future stopFuture) throws Exception {
        System.out.println("MyVerticle stopped!");
    }

}