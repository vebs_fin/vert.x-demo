package rohit.myverticle

import io.vertx.core.AbstractVerticle
import io.vertx.core.Future

/**
 * Created by nexthought on 12/16/17.
 */
class RandomVerticle extends AbstractVerticle {
    final String lexicon = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";

    final java.util.Random rand = new java.util.Random();

// consider using a Map<String,Boolean> to say whether the identifier is being used or not
    final Set<String> identifiers = new HashSet<String>();

    @Override
    public void start(Future<Void> startFuture) {
        println "111111111111" + randomIdentifier()
        println "2222222222222" + randomIdentifier()
        println "111111111111" + randomIdentifier()
        println "111111111111" + randomIdentifier()
        println "111111111111" + randomIdentifier()
        println "111111111111" + randomIdentifier()
        println "111111111111" + randomIdentifier()
        println "111111111111" + randomIdentifier()
        println "111111111111" + randomIdentifier()
    }

    @Override
    public void stop(Future stopFuture) throws Exception {
        System.out.println("MyVerticle stopped!");
    }


    public String randomIdentifier() {
        StringBuilder builder = new StringBuilder();
        while (builder.toString().length() == 0) {
            int length = rand.nextInt(5) + 5;
            for (int i = 0; i < length; i++) {
                builder.append(lexicon.charAt(rand.nextInt(lexicon.length())));
            }
            if (identifiers.contains(builder.toString())) {
                builder = new StringBuilder();
            }
        }
        return builder.toString();
    }

}