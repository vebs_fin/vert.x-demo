package rohit.myverticle

import io.vertx.core.AbstractVerticle
import io.vertx.core.AsyncResult
import io.vertx.core.Future
import io.vertx.core.Handler
import io.vertx.core.eventbus.Message

public class EventBusReceiverVerticle extends AbstractVerticle {

    private String name = null;

    public EventBusReceiverVerticle(String name) {
        this.name = name;
    }

    public void start(Future<Void> startFuture) {

//        vertx.eventBus().consumer("anAddress", message -> {
//            System.out.println(this.name +
//                    " received message: " +
//                    message.body());
//        });

        vertx.eventBus().consumer("anAddress", new Handler<Message<String>>() {
            @Override
            public void handle(Message<String> message) {
                System.out.println(
                        " received message: " +
                        message.body());
            }
        });
    }
}
