package rohit.myclass

import io.vertx.core.AsyncResult
import io.vertx.core.Handler

//import io.vertx.core.Handler
import io.vertx.core.Vertx
import io.vertx.ext.mail.MailClient
import rohit.myverticle.EventBusReceiverVerticle
import rohit.myverticle.EventBusSenderVerticle
import rohit.myverticle.MailVerticle
import rohit.myverticle.MyVerticle
import rohit.myverticle.RandomVerticle


/**
 * Created by nexthought on 12/15/17.
 */
class MyApp {
    public static void main(String[] args) {
        Vertx vertx = Vertx.vertx();
        vertx.deployVerticle(new MyVerticle(), new Handler<AsyncResult<String>>() {
            @Override
            public void handle(AsyncResult<String> stringAsyncResult) {
                System.out.println("BasicVerticle deployment complete");
            }
        });
        println "111111111111111111111111111111"


        vertx.deployVerticle(new EventBusReceiverVerticle("R1"));
        vertx.deployVerticle(new EventBusReceiverVerticle("R2"));

        Thread.sleep(3000);
        vertx.deployVerticle(new EventBusSenderVerticle());
        vertx.deployVerticle(new MailVerticle());
        vertx.deployVerticle(new RandomVerticle());








    }
}
