package com.bookazon.web

import com.bookazon.router.verticle.MasterVerticle
import io.vertx.core.Vertx

class WebInterface {
    public static void main(String... args) {
        Vertx vertx = Vertx.vertx()
        vertx.deployVerticle(new MasterVerticle())
    }
}
