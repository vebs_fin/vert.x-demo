package com.bookazon.models

class Book {
    String bookID
    String name
    String publisher
    String ISBN
    String yearOfPublication
    String bookPrice
    String rating
    String author
    String uuid

    Book(){

    }

    Book(String bookID, String name, String publisher, String ISBN, String yearOfPublication, String bookPrice, String rating, String author) {
        this.bookID = bookID
        this.name = name
        this.publisher = publisher
        this.ISBN = ISBN
        this.yearOfPublication = yearOfPublication
        this.bookPrice = bookPrice
        this.rating = rating
        this.author = author
    }
}

